*Roku is a simple, minimal library which gives you the ability to turn any method in your C# application into a REST endpoint*

#### Prerequisite 
This library requires you to use a DI system and to have a basic understanding of how it works.

### Setup
- Install `Roku` package from nuget in *one* of the following ways:
  - From Rider or Visual Studio Nuget window
  - If you're using dotnet in the console
    - `dotnet add package Roku`
  - From the package manager console:
    - `Install-Package Roku`
- Add `Roku` to your `ServiceCollection`
```cs
_svcCollection.AddRoku(new RokuConfig() {
  // configure options here
});
```
- Run `Roku` after building the `ServiceProvider`  

*Convenience method for simplicity*
```cs
await _svcs.RunRokuAsync();
````
*Recommended usage*
```cs
var roku = _svcs.GetRequiredService<IRokuService>();
// starts the Kestrel server which will listen to requests and waits for it to stop (indefinitely)
await roku.RunAsync(waitForShutdown: true);
```
- Mark classes whose methods you want to expose via API with `[Roku<T>]`
  - `<T>` is the type you've registered your service with (see example)
- Mark your method with `[RokuEndpoint]`
*Example:*
```cs
namespace Test;

public interface ITestClass
{
    string Ping();
}

// we will put ITestClass here because that's how we register it 
// in the service collection.
[Roku<ITestClass>]
public class TestClass : ITestClass
{
    [RokuEndpoint]
    public string Ping()
    {
        return "pong";
    }
}
```
- Add it to your service collection 
```cs
_svcCollection.AddSingleton<ITestClass, TestClass>();
```
- Start your app. For each marked method, you will have a REST endpoint available on `localhost:40608/<Namespace>.<RegisteredTypeName>/<MethodName>`
  - In the example above, you will be able to do a `GET localhost:40608/Testing.ITestClass/Ping`
  - The server will reply with 
    - 200 OK: `"pong"`

### Features
- Opening `localhost:40608/` in your browser will show you the list of available endpoints
- Port is configurable
- Response headers are configurable
- Endpoints which receive parameters are POST, not GET
  - Parameters are passed as a Json Array.
  - You're able to pass your own `JsonSerializerOptions` for custom deserialization and type handling.
  - Make sure your client POSTs expected json in the proper format.
- You're able to provide your own cert for HTTPS support.
- You're able to provide a Secret Key for endpoint protection (only use with HTTPS enabled)

```csharp
namespace MyProgram;

public class Program
{
    public static async Task Main(string[] args)
    {
        var svcs = new ServiceCollection()
            // add roku service, config, and autodiscover endpoints
            .AddRoku() 
            // register the class which has our endpoints
            .AddSingleton<ITestClass, TestClass>() 
            .BuildServiceProvider();
        
        // get the roku service
        var roku = svcs.GetRequiredService<IRokuService>()

        // run it and wait for shutdown
        await roku.RunAsync(waitForShutdown: true);
    }

    public interface ITestClass
    {
        string Ping();
    }

    [Roku<TestClass>]
    public sealed class TestClass : ITestClass
    {
        [RokuEndoint]
        public string Ping()
        {
            Console.WriteLine("Ping executed!");
            return "pong";
        }
    }
}
```

*Roku is NOT an asp.net replacement. It is meant to be used in applications where you simply want to expose some preexisting methods as REST endpoints.*
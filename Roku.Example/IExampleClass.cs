namespace Roku;

public interface IExampleClass
{
    // The methods don't have to be specified in the interface.
    // It is only important to specify the correct Registration Type (T) in the Roku<T> Attribute.
}
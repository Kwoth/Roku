namespace Roku;

public class ExampleObject
{
    public string StrProp { get; set; } = "This should be in the json output";
    public int IntProp { get; set; } = 46;
}
namespace Roku;

[Roku<IExampleClass>]
public sealed class ExampleClass : IExampleClass
{
    [RokuEndpoint]
    public async ValueTask<string> ExecMe()
    {
        await new ValueTask<bool>(true);
        return "Value Task definitely works";
    }
        
    [RokuEndpoint]
    public async Task<string> TaskWorks()
    {
        await Task.CompletedTask;
        return "Task definitely works";
    }
        
    [RokuEndpoint]
    public string StringWorks()
    {
        return "String definitely works";
    }
        
    [RokuEndpoint]
    public ExampleObject ObjectWorks()
    {
        return new();
    }

    [RokuEndpoint]
    public string Ping(int message, string? extra = null)
    {
        return message + " and " + extra;
    }

    [RokuEndpoint]
    public string CustomParser(ExampleObject obj)
    {
        return obj.StrProp + ": " + obj.IntProp;
    }
        
    [RokuEndpoint]
    public int Increment(int input)
    {
        Console.WriteLine($"input value received: {input}");
        return input + 1;
    }
}
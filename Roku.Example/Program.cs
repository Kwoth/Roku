﻿using Microsoft.Extensions.DependencyInjection;

namespace Roku;

public class Program
{
    public static async Task Main(string[] args)
    {
        var services = new ServiceCollection()
            .AddSingleton<IExampleClass, ExampleClass>()
            .AddRoku(new ()
            {
                // Whether all available endpoints will be listed on 'GET /' request
                EnableEndpointsList = true,
                
                // If set, enables https. Make sure that the client trusts it.
                // CertPath = "cert.cer",
                // CertPassword = "123",
                
                // //
                // // Sets response headers. By default roku only sets 'Host: nginx'
                // //
                // // - Sucess responses will have Content-Type 'application/json', unless you change it here
                // //   however the responses will still be in the json format
                // //
                // // - Error responses (4xx and 5xx) will have Contet-Type 'text/plain' with an error message in the body
                // //   (cannot be changed)
                // ResponseHeaders = new[]
                // {
                //     ("Access-Control-Allow-Origin", "*")
                // },
                
                // // Set desired port. Default 40608
                // Port = 40608
                
                // // Optional secret key. Clients sending request will have to send that key in the 
                // // 'Roku-Secret-Key' Header
                // // You ***__MUST__*** use HTTPS when using this property
                // SecretKey = "hhRV&R9&6SsgbW7sCANEt2gXV!mp83P%!f@Pg!iaHQ%46bfWBE"
            })
            .BuildServiceProvider();

        // convenience method for Loading
        await services.LoadAndRunRokuAsync();
        
        // // or 
        // var roku = services.GetRequiredService<IRokuService>();
        // roku.LoadEndpointsFromAssembly(Assembly.GetExecutingAssembly());
        // await roku.RunAsync(waitForShutdown: true);
    }
}
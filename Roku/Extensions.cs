﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Roku;

public static class Extensions
{
    public static IServiceCollection AddRoku(this IServiceCollection service,
        RokuConfig? config = default)
    {
        return service
            .AddSingleton<IRokuService, RokuService>()
            .AddSingleton<RokuConfig>(config ?? new());
    }

    public static async Task<IRokuService> LoadAndRunRokuAsync(this IServiceProvider services,
        bool waitForShutdown = true,
        CancellationToken cancel = default)
    {
        var roku = services.GetRequiredService<IRokuService>();
        roku.LoadEndpointsFromAssembly(Assembly.GetEntryAssembly() ??
                                       throw new InvalidOperationException(
                                           "Entry assembly not found. Specify the Assembly manually."));


        await roku.RunAsync(waitForShutdown: waitForShutdown, cancel);
        return roku;
    }
}
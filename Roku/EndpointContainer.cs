﻿using System.Collections.Immutable;
using System.Reflection;

namespace Roku;

public readonly struct EndpointData
{
    public EndpointData(
        Type registeredType,
        MethodInfo method)
    {
        Method = method;
        RegisteredType = registeredType;
        
        var paramInfos = method.GetParameters();
        var cmdParams = new ParamData[paramInfos.Length];

        for (var paramCounter = 0; paramCounter < paramInfos.Length; paramCounter++)
        {
            var pi = paramInfos[paramCounter];

            var paramName = pi.Name;
            var hasDefaultValue = pi.HasDefaultValue;
            var isParams = pi.GetCustomAttribute<ParamArrayAttribute>() != null;
            var paramType = pi.ParameterType;

            if (isParams)
            {
                // if it's params, it means it's an array, and i only need a parser for the actual type,
                // as the parser will run on each array element, it can't be null
                paramType = paramType.GetElementType()!;
            }

            cmdParams[paramCounter] = new ParamData(paramName,
                paramType,
                hasDefaultValue,
                isParams);
        }

        Params = cmdParams.ToImmutableArray();
    }
    
    /// <summary>
    /// Type this endpoint's containing type is registered as
    /// </summary>
    public Type RegisteredType { get; }
    
    /// <summary>
    /// List of parameter information required for parsing
    /// </summary>
    public IReadOnlyList<ParamData> Params { get; } 
    
    /// <summary>
    /// Method to be executed
    /// </summary>
    public MethodInfo Method { get; }

    public string HttpMethod
        => Method.GetParameters().Length > 0
            ? "POST"
            : "GET";
}

public readonly struct ParamData
{
    public string? ParamName { get; }
    public Type ParamType { get; }
    public bool HasDefaultValue { get; }
    public bool IsParams { get; }

    public ParamData(string? paramName, Type paramType, bool hasDefaultValue, bool isParams)
    {
        ParamName = paramName;
        ParamType = paramType;
        HasDefaultValue = hasDefaultValue;
        IsParams = isParams;
    }
}
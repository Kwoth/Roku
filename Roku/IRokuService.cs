﻿using System.Reflection;

namespace Roku;

public interface IRokuService
{
    IReadOnlyDictionary<string, EndpointData> Endpoints { get; }
    void LoadEndpointsFromAssembly(Assembly getEntryAssembly);
    Task RunAsync(bool waitForShutdown, CancellationToken cancel = default);
}
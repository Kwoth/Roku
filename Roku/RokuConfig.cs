using System.Net;
using System.Text.Json;

namespace Roku;

public class RokuConfig
{
    /// <summary>
    /// Cert to use for https. If set, enables https.
    /// </summary>
    public string? CertPath { get; init; } = null;
    
    /// <summary>
    /// Optional password for your cert
    /// </summary>
    public string? CertPassword { get; init; } = null;

    /// <summary>
    /// Whether all available endpoints will be listed on GET / 
    /// </summary>
    public bool EnableEndpointsList { get; init; } = false;

    /// <summary>
    /// Which headers the server will return 
    /// </summary>
    public IReadOnlyList<(string, string)>? ResponseHeaders { get; init; } = null;

    /// <summary>
    /// Set port the server will listen on. Default 40608
    /// </summary>
    public int? Port { get; init; } = null;

    /// <summary>
    /// Secret key that the server will except in 'Roku-Secret-Key' haader with each request.
    /// USE ONLY IF HTTPS IS ENABLED (certPath is set)
    /// </summary>
    public string? SecretKey { get; init; } = null;

    /// <summary>
    /// Optional custom deserializing behavior
    /// </summary>
    public JsonSerializerOptions? DeserializerOptions { get; init; } = null;

    /// <summary>
    /// Address the kestrel server will listen on. Defaultis IPAdress.Any
    /// </summary>
    public IPAddress? ListenAddress { get; set; } = null;
}
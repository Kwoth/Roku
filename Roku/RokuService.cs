﻿using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Roku;

internal sealed class RokuService : IRokuService
{
    public IReadOnlyDictionary<string, EndpointData> Endpoints
        => _endpoints;

    private readonly Dictionary<string, EndpointData> _endpoints = new();

    private readonly IServiceProvider _services;
    private readonly RokuConfig _conf;
    private readonly ILogger<RokuService>? _logger;

    public RokuService(IServiceProvider services,
        RokuConfig conf,
        ILogger<RokuService>? logger = null)
    {
        _services = services;
        _conf = conf;
        _logger = logger;
    }

    public void LoadEndpointsFromAssembly(Assembly a)
    {
        var types = a.ExportedTypes
            .Where(x => x.IsClass && !x.IsAbstract)
            .Where(x => CustomAttributeData.GetCustomAttributes(x).Any(att
                => att.AttributeType.IsGenericType &&
                   att.AttributeType.GetGenericTypeDefinition() == typeof(Roku<>)));

        foreach (var type in types)
        {
            var methods = type.GetMethods()
                .Where(mi => CustomAttributeData.GetCustomAttributes(mi)
                    .Any(att => att.AttributeType == typeof(RokuEndpoint)));

            var registeredType = CustomAttributeData.GetCustomAttributes(type)
                .First(x => x.AttributeType.IsGenericType &&
                            x.AttributeType.GetGenericTypeDefinition() == typeof(Roku<>))
                .AttributeType
                .GenericTypeArguments[0];

            foreach (var method in methods)
            {
                var endpointName = GetFullEndpointName(registeredType, method);
                _endpoints.Add(endpointName,
                    new EndpointData(registeredType, method));
            }
        }
    }

    private string GetFullEndpointName(Type type, MethodInfo method)
        => $"{type.Namespace}.{type.Name}/{method.Name}";

    public async Task RunAsync(bool waitForShutdown = true, CancellationToken cancel = default)
    {
        var builder = CreateWebHostBuilder();

        var host = builder.Build();
        await host.StartAsync(cancel);

        _logger?.LogInformation("Roku is now listening on {Address}:{Port}",
            _conf.ListenAddress ?? IPAddress.Any,
            _conf.Port ?? 40608);
        if (waitForShutdown)
        {
            await host.WaitForShutdownAsync(cancel);
        }
    }

    private IWebHostBuilder CreateWebHostBuilder()
    {
        var builder = new WebHostBuilder();
        
        builder.UseKestrel((_, options) =>
            {
                options.Listen(_conf.ListenAddress ?? IPAddress.Any,
                    _conf.Port ?? 40608,
                    lo =>
                    {
                        // use https if a cert is provided
                        if (!string.IsNullOrWhiteSpace(_conf.CertPath))
                            lo.UseHttps(_conf.CertPath, _conf.CertPassword);
                    });
            })
            .ConfigureServices((_, services) =>
            {
                // FUTURE? is there a way to have simple host filtering
                services.AddRouting();
            })
            .Configure(app =>
            {
                app.UseRouting();

                // check for the secret key
                if (_conf.SecretKey is string secretKey)
                {
                    app.Use(async (ctx, next) =>
                    {
                        if (!ctx.Request.Headers.TryGetValue("Roku-Secret-Key", out var val)
                            || !SecretCorrect(val, secretKey))
                        {
                            ctx.Response.StatusCode = StatusCodes.Status403Forbidden;
                            await ctx.Response.CompleteAsync();
                            return;
                        }

                        await next();
                    });
                }

                // add specified headers, or default ones if none specified
                app.Use(async (ctx, next) =>
                {
                    ctx.Response.Headers.ContentType = "application/json";
                    
                    // pretend it's nginx
                    ctx.Response.Headers.Server = "nginx";
                    // if config is not setup, just set the default
                    if (_conf.ResponseHeaders is not null)
                    {
                        foreach (var (header, value) in _conf.ResponseHeaders)
                        {
                            ctx.Response.Headers[header] = value;
                        }
                    }

                    await next();
                });

                app.UseEndpoints(eb =>
                {
                    using var scope = _services.CreateScope();

                    foreach (var (key, value) in Endpoints)
                    {
                        var endpointPath = $"/{key}";

                        if (value.Method.GetParameters().Length == 0)
                        {
                            _logger?.LogInformation("Registering GET {EndpointPath}", endpointPath);
                            eb.MapGet(endpointPath, GetRequestHandler(value));
                        }
                        else
                        {
                            _logger?.LogInformation("Registering POST {EndpointPath}", endpointPath);
                            eb.MapPost(endpointPath, PostRequestHandler(value));
                        }
                    }

                    if (_conf.EnableEndpointsList)
                    {
                        eb.Map("/",
                            async req =>
                            {
                                req.Response.StatusCode = 200;
                                req.Response.ContentType = "text/html";

                                var sb = new StringBuilder();

                                sb.AppendLine(@"<html>
<head>
    <title> Roku Endpoints </title>
    <style>* { background: #eee; font-family: 'Helvetica'; }</style>
<head>
<body>
<table>
  <tr>
    <th>Method</th>
    <th>Endpoint</th>
    <th>Parameters</th>
  </tr>");

                                foreach (var ep in _endpoints)
                                {
                                    sb.AppendLine("<tr>");
                                    sb.AppendLine($"<td>{ep.Value.HttpMethod}</td>");
                                    sb.AppendLine($"<td><a target=\"_blank\" href=\"/{ep.Key}\">/{ep.Key}</a></td>");
                                    sb.AppendLine($"<td>({string.Join(", ", ep.Value.Params.Select(x => $"{x.ParamType} {x.ParamName}"))})</td>");
                                    sb.AppendLine("</tr>");
                                }
                                
                                await req.Response.WriteAsync(sb.ToString());
                                
                                sb.AppendLine(@"</table>
</body>
</html>");

                                await req.Response.CompleteAsync();
                            });
                    }
                });
            });

        return builder;
    }

    private bool SecretCorrect(string userInput, string secretKey)
    {
        ReadOnlySpan<byte> userInputBytes = Encoding.UTF8.GetBytes(userInput);
        ReadOnlySpan<byte> secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);

        ReadOnlySpan<byte> userInputHash = SHA512.HashData(userInputBytes);
        ReadOnlySpan<byte> secretKeyHash = SHA512.HashData(secretKeyBytes);

        return SlowEquals(userInputHash, secretKeyHash)
               && SlowEquals(userInputBytes, secretKeyBytes);
    }

    private static bool SlowEquals(ReadOnlySpan<byte> first, ReadOnlySpan<byte> second)
    {
        var diff = first.Length ^ second.Length;
        for (var i = 0; i < first.Length && i < second.Length; i++)
        {
            diff |= first[i] ^ second[i];
        }

        return diff == 0;
    }


    async Task RequestHandler(HttpContext ctx, EndpointData data,
        Func<HttpContext, EndpointData, object, Task> ExecuteAction)
    {
        await using var scope = _services.CreateAsyncScope();
        try
        {
            var service = scope.ServiceProvider.GetService(data.RegisteredType);
            if (service is null)
            {
                await SetErrorResponse(ctx.Response,
                    "Service of type {ServiceType} not found. " +
                    "Make sure that your service is registered with the type specified as T in the RokuExec<T>",
                    StatusCodes.Status500InternalServerError,
                    null);
                return;
            }

            try
            {
                _logger?.LogInformation("Executing endpoint...");

                await ExecuteAction(ctx, data, service);
            }
            catch (ArgumentException ex)
            {
                // the type handling system should check if the types are correct
                // and if the signature is satisfied, therefore
                // if this error happens, that means the tpye handler didn't do it's
                // job properly in reporting a client input error, or it has a bug

                await SetErrorResponse(ctx.Response,
                    "There was a server error with passing data to the endpoint.",
                    StatusCodes.Status500InternalServerError,
                    ex);
            }
            catch (TargetInvocationException ex)
            {
                await SetErrorResponse(ctx.Response,
                    "There was an error executing the endpoint",
                    StatusCodes.Status500InternalServerError,
                    ex);
            }
            catch (TargetParameterCountException ex)
            {
                // invalid number of arguments, although this should be picked up 
                // by the type handler

                await SetErrorResponse(ctx.Response,
                    "You've provided an incorrect number of arguments.",
                    StatusCodes.Status400BadRequest,
                    ex);
            }
            catch (Exception ex)
            {
                await SetErrorResponse(ctx.Response,
                    "Internal server error.",
                    StatusCodes.Status500InternalServerError,
                    ex);
            }

            // send response
            await ctx.Response.CompleteAsync();
        }
        catch (Exception ex)
        {
            await SetErrorResponse(ctx.Response,
                "Unexpected internal server error. Please report this.",
                StatusCodes.Status500InternalServerError,
                ex);

            Console.WriteLine(ex.ToString());
            await ctx.Response.CompleteAsync();
        }
    }

    private async Task SetErrorResponse(HttpResponse ctxResponse,
        string errorMessage,
        int statusCode,
        Exception? ex)
    {
        _logger?.LogWarning(ex,
            "An error occurred while executing an endpoint: {StatusCode} {ErrorMessage}",
            statusCode,
            errorMessage);

        ctxResponse.Headers["Content-Type"] = "text/plain";
        await ctxResponse.WriteAsync(errorMessage);
        ctxResponse.StatusCode = statusCode;
    }

    Func<HttpContext, Task> GetRequestHandler(EndpointData epData)
        => ctx => RequestHandler(ctx, epData, GetRequestAction);

    Func<HttpContext, Task> PostRequestHandler(EndpointData epData)
        => ctx => RequestHandler(ctx, epData, PostRequestAction);

    private async Task PostRequestAction(HttpContext ctx,
        EndpointData data,
        object service)
    {
        // get args
        var args = await GetParsedArgsAsync(ctx.Request, data);
        var retValue = data.Method.Invoke(service, args);

        // set response
        ctx.Response.StatusCode = 200;
        var response = JsonSerializer.Serialize(retValue);
        await ctx.Response.WriteAsJsonAsync(response);
    }

    private async Task GetRequestAction(HttpContext ctx,
        EndpointData data,
        object service)
    {
        var value = await InvokeMethodAsync(data, service, null);

        ctx.Response.StatusCode = 200;
        await WriteResponseAsync(ctx, value);
    }

    private async Task WriteResponseAsync(HttpContext ctx, object? value)
    {
        await ctx.Response.WriteAsJsonAsync(value);
    }

    private async Task<object?> InvokeMethodAsync(EndpointData data, object targetObject, object?[]? args)
    {
        var returnVal = data.Method.Invoke(targetObject, args);

        var returnType = data.Method.ReturnType;

        if (returnVal is null)
            return null;

        if (returnType == typeof(Task))
        {
            // just a normal task. No return value
            await (Task)returnVal;
            return null;
        }

        if (returnType == typeof(ValueTask))
        {
            // just a normal value task. No return value
            await (ValueTask)returnVal;
            return null;
        }

        if (returnType.IsGenericType)
        {
            if (returnType.GetGenericTypeDefinition() == typeof(Task<>))
            {
                await (Task)returnVal;

                var prop = returnType.GetProperty("Result")!;
                return prop.GetValue(returnVal);
            }
            else if (returnType.GetGenericTypeDefinition() == typeof(ValueTask<>))
            {
                // get the .AsTask method on ValueTask<T>
                // this will return a Task<T>
                var method = returnType.GetMethod("AsTask")!;
                var task = method.Invoke(returnVal, null)!;

                // await that Task<T> by casting it to Task
                await (Task)task;

                // Get the Result property and return it
                var prop = task.GetType().GetProperty("Result")!;
                return prop.GetValue(task);
            }
        }

        // FUTURE? there should be a check for custom awaitables maybe

        // if the return value is not a Task, it must be a data type - return it
        return returnVal;
    }

    private async Task<object?[]?> GetParsedArgsAsync(HttpRequest req, EndpointData epi)
    {
        // first interpret body as a json string array. That's what's expected
        var stringArgs = await JsonSerializer.DeserializeAsync<string?[]>(req.Body);

        return await ParseArgsAsync(stringArgs ?? Array.Empty<string>(), epi);
    }

    private async Task<object?[]?> ParseArgsAsync(string?[] argsArray, EndpointData data)
    {
        var paramObjs = new object?[data.Params.Count];

        // in normal circumastance (no optional parameters)
        // args have to match exactly the length of the parameters
        var minArgs = data.Params.Count;
        var maxArgs = data.Params.Count;

        if (data.Params.Count > 0)
        {
            // if the last parameter is params, the number of maximum arguments provided
            // is theoretically infinite, as all of the extra ones will be merged and put into the last parameter
            var lastParam = data.Params.ElementAt(data.Params.Count - 1);
            if (lastParam.IsParams)
            {
                maxArgs = int.MaxValue;
            }

            // minimum number of arguments that has to be provided is reduced by
            // 1 for each parameter with default value
            var defaultCount = data.Params.Count(x => x.HasDefaultValue || x.IsParams);
            minArgs -= defaultCount;
        }

        // args length has to be [minargs, maxargs]
        if (argsArray.Length < minArgs || argsArray.Length > maxArgs)
            return null;

        for (int i = 0; i < data.Params.Count; i++)
        {
            var param = data.Params[i];

            // means we ran out of arguments to fill into parameters
            if (i == argsArray.Length)
            {
                if (param.HasDefaultValue)
                {
                    paramObjs[i] = Type.Missing;
                    continue;
                }
                else if (param.IsParams)
                {
                    paramObjs[i] = null;
                    continue;
                }

                // this should never happen?
                return null;
            }

            if (param.IsParams)
            {
                // i'll take all leftover args, and run the current parser on them

                var results = await Task.WhenAll(argsArray
                    .Skip(i)
                    .Select(async arg => arg is null
                        ? arg
                        : await Deserialize(arg, param.ParamType)));

                paramObjs[i] = results;
                return paramObjs;
            }

            var elem = argsArray[i];
            var result = elem is null
                ? elem
                : await Deserialize(elem, param.ParamType);
            
            paramObjs[i] = result;
        }

        return paramObjs;
    }

    private ValueTask<object?> Deserialize(string arg, Type argType)
    {
        object? result;
        try
        {
            result = JsonSerializer.Deserialize(arg, argType, _conf.DeserializerOptions);
        }
        catch (Exception ex)
        {
            _logger?.LogError(ex,
                "An exception ocurred while deserializing {Arg} value to an object of {Type} type",
                arg,
                argType);
            throw;
        }
        return new(result);
    }
}